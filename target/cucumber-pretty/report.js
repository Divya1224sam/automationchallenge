$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("HomeLoanEnquiry.feature");
formatter.feature({
  "line": 2,
  "name": "HomeLoanEnquiry",
  "description": "",
  "id": "homeloanenquiry",
  "keyword": "Feature"
});
formatter.before({
  "duration": 2643101700,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Verify the new home loan enquiry form is successfully submitted from the NAB website for a new customer",
  "description": "",
  "id": "homeloanenquiry;verify-the-new-home-loan-enquiry-form-is-successfully-submitted-from-the-nab-website-for-a-new-customer",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I launch the selected browser",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to the NAB Home page",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "the NAB Home page is loaded successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I click on the home loans link on the home page",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "the home loans page is loaded successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I click on request a call back option on the home loan page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "the customer assistance directory page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I select the enquiry about New Home Loans option and proceed",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "the NAB call back form is loaded successfully in a new window",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I enter the customer details in the call back form as",
  "rows": [
    {
      "cells": [
        "FirstName",
        "LastName",
        "State",
        "PhoneNumber",
        "Email",
        "ExistingCustomer",
        "NabID"
      ],
      "line": 15
    },
    {
      "cells": [
        "Test",
        "New",
        "NSW",
        "0444444444",
        "testnew@gmail.com",
        "No",
        ""
      ],
      "line": 16
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I verify the call back form is submitted successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iLaunchTheGivenBrowser()"
});
formatter.result({
  "duration": 141902900,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iNavigateToNabHomePage()"
});
formatter.result({
  "duration": 3728503700,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isNabHomePageLoaded()"
});
formatter.result({
  "duration": 7146200,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iClickOnHomeLoansLink()"
});
formatter.result({
  "duration": 14673273400,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isHomeLoanPageLoaded()"
});
formatter.result({
  "duration": 3566100,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iClickOnRequestForCallBackOption()"
});
formatter.result({
  "duration": 1190801900,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isCustomerAssistanceDirectoryPageLoaded()"
});
formatter.result({
  "duration": 9776200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Home Loans",
      "offset": 27
    }
  ],
  "location": "HomeLoanEnquirySteps.iSelectNewHomeLoanRadioButton(String)"
});
formatter.result({
  "duration": 416675600,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isNabCallBackFormPageLoaded()"
});
formatter.result({
  "duration": 883180500,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iEnterTheCustomerDetailsOnCallBackForm(DataTable)"
});
formatter.result({
  "duration": 2520214000,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iVerifyCallBackSubmitSuccessMessage()"
});
formatter.result({
  "duration": 1106839000,
  "status": "passed"
});
formatter.after({
  "duration": 670455500,
  "status": "passed"
});
formatter.before({
  "duration": 1368218100,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Verify the new home loan enquiry form is successfully submitted from the NAB website for an existing customer",
  "description": "",
  "id": "homeloanenquiry;verify-the-new-home-loan-enquiry-form-is-successfully-submitted-from-the-nab-website-for-an-existing-customer",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "I launch the selected browser",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "I navigate to the NAB Home page",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "the NAB Home page is loaded successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I click on the home loans link on the home page",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "the home loans page is loaded successfully",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "I click on request a call back option on the home loan page",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "the customer assistance directory page is displayed",
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "I select the enquiry about New Home Loans option and proceed",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "the NAB call back form is loaded successfully in a new window",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I enter the customer details in the call back form as",
  "rows": [
    {
      "cells": [
        "FirstName",
        "LastName",
        "State",
        "PhoneNumber",
        "Email",
        "ExistingCustomer",
        "NabID"
      ],
      "line": 30
    },
    {
      "cells": [
        "Test",
        "User",
        "QLD",
        "0445544444",
        "testuser@gmail.com",
        "Yes",
        "12457865"
      ],
      "line": 31
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I verify the call back form is submitted successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iLaunchTheGivenBrowser()"
});
formatter.result({
  "duration": 1491500,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iNavigateToNabHomePage()"
});
formatter.result({
  "duration": 3733538200,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isNabHomePageLoaded()"
});
formatter.result({
  "duration": 6390500,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iClickOnHomeLoansLink()"
});
formatter.result({
  "duration": 15193630000,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isHomeLoanPageLoaded()"
});
formatter.result({
  "duration": 5630500,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iClickOnRequestForCallBackOption()"
});
formatter.result({
  "duration": 1196196000,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isCustomerAssistanceDirectoryPageLoaded()"
});
formatter.result({
  "duration": 12155600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Home Loans",
      "offset": 27
    }
  ],
  "location": "HomeLoanEnquirySteps.iSelectNewHomeLoanRadioButton(String)"
});
formatter.result({
  "duration": 504208400,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.isNabCallBackFormPageLoaded()"
});
formatter.result({
  "duration": 1038249000,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iEnterTheCustomerDetailsOnCallBackForm(DataTable)"
});
formatter.result({
  "duration": 2570024200,
  "status": "passed"
});
formatter.match({
  "location": "HomeLoanEnquirySteps.iVerifyCallBackSubmitSuccessMessage()"
});
formatter.result({
  "duration": 838261400,
  "status": "passed"
});
formatter.after({
  "duration": 717472800,
  "status": "passed"
});
formatter.uri("WeatherAPITests.feature");
formatter.feature({
  "line": 2,
  "name": "Weather API Tests",
  "description": "",
  "id": "weather-api-tests",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4321900,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "User generates token for Authorisation",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am an authorized user",
  "keyword": "Given "
});
formatter.match({
  "location": "WeatherAPISteps.iAmAnAuthorizedUser()"
});
formatter.result({
  "duration": 347968100,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Verify and validate the response of the GET request for the current weather data",
  "description": "",
  "id": "weather-api-tests;verify-and-validate-the-response-of-the-get-request-for-the-current-weather-data",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 9,
  "name": "the latitude 40.730610 and longitude -73.935242 details for a region",
  "keyword": "Given "
});
formatter.step({
  "line": 10,
  "name": "the current weather endpoint is called",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "the API should return a valid response status",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "the API response contains the state code as NY",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "40.730610",
      "offset": 13
    },
    {
      "val": "-73.935242",
      "offset": 37
    }
  ],
  "location": "WeatherAPISteps.latitudeAndLongitudeDetails(String,String)"
});
formatter.result({
  "duration": 47300,
  "status": "passed"
});
formatter.match({
  "location": "WeatherAPISteps.getCurrentWeatherEndpoint()"
});
formatter.result({
  "duration": 1129255300,
  "status": "passed"
});
formatter.match({
  "location": "WeatherAPISteps.theAPIShouldReturnAResponse()"
});
formatter.result({
  "duration": 74400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NY",
      "offset": 44
    }
  ],
  "location": "WeatherAPISteps.theAPIResponseParser(String)"
});
formatter.result({
  "duration": 24457100,
  "status": "passed"
});
formatter.after({
  "duration": 20800,
  "status": "passed"
});
});