package helper;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class XMLHelper {
    private static String DEFAULT_CONFIG_FILE = "src/test/java/properties/default_config.xml";
    private static Map<String, String> defaultEnvProperties;

    public static void loadDefaultProperties() {

        File xmlFile = new File(DEFAULT_CONFIG_FILE);
        if (defaultEnvProperties == null)
            defaultEnvProperties = new HashMap<>();
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(xmlFile);
            NodeList nodes = doc.getElementsByTagName("config");
            Node configNode = nodes.item(0);
            NodeList configValueNodes = configNode.getChildNodes();
            for (int index = 0; index < configValueNodes.getLength(); index++) {
                Node node = configValueNodes.item(index);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element nodeEle = (Element) node;
                    String propertyName = nodeEle.getNodeName();
                    String propNewValue = nodeEle.getTextContent();
                    if (System.getProperty(propertyName) == null) {
                        defaultEnvProperties.put(propertyName, propNewValue);
                        System.setProperty(propertyName, propNewValue);
                    } else {
                        defaultEnvProperties.put(propertyName, System.getProperty(propertyName));
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
