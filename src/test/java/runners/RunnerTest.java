package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "./src/test/java/features",
        plugin = {"pretty:STDOUT", "html:target/cucumber-pretty",
                "json:target/cucumber-reports/cucumber.json",
                "junit:target/cucumber-reports/cucumberjunit.xml",
        },
        glue = {"steps"}

)
public class RunnerTest {

    @BeforeClass
    public static void main()
    {
        System.out.println("Automation Test Execution Started");
    }
}
