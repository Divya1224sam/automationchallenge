package api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class APIManager {

    public static APIHandler getWeatherAPIHandler() {
        APIHandler weatherAPIHandler = new APIHandler();
        RequestSpecBuilder specBuilder = new RequestSpecBuilder();
        specBuilder.setBaseUri(System.getProperty("apiBaseUrl"));
        weatherAPIHandler.setSpec(specBuilder.build());
        return weatherAPIHandler;
    }

    public static Response executeGETCurrentWeather(APIHandler apiHandler, String urlToAppend) {
        Response response = null;
        int retryCount = 1;
        do {
            response = given()
                    .spec(apiHandler.getSpec())
                    .when().get(urlToAppend)
                    .then()
                    .extract().response();
            if (response.getStatusCode() == 200) {
                return response;
            }
            retryCount++;
        } while (retryCount < 3);
        return response;
    }
}
