package api;

import io.restassured.specification.RequestSpecification;


public class APIHandler {
        private RequestSpecification spec;

        public RequestSpecification getSpec() {
            return spec;
        }

        public void setSpec(RequestSpecification spec) {
            this.spec = spec;
        }
}
