package api;
import io.restassured.response.Response;

public class WeatherAPI {
    private Response response = null;
    private APIHandler weatherAPIHandler = null;

    private static final String request_url_current_weather = "/current";


    public WeatherAPI() {
        if (weatherAPIHandler == null)
            weatherAPIHandler = APIManager.getWeatherAPIHandler();
    }

    public Response getCurrentWeather(String lat, String lon) {
        String request_url = request_url_current_weather + "?lat=" + lat  + "&lon=" +lon +"&key="+System.getProperty("apiKey");
        response = APIManager.executeGETCurrentWeather(weatherAPIHandler, request_url);
        return response;
    }
}
