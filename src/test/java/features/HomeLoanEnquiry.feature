
Feature: HomeLoanEnquiry

  Scenario: Verify the new home loan enquiry form is successfully submitted from the NAB website for a new customer
    Given I launch the selected browser
    And I navigate to the NAB Home page
    Then the NAB Home page is loaded successfully
    When I click on the home loans link on the home page
    Then the home loans page is loaded successfully
    When I click on request a call back option on the home loan page
    Then the customer assistance directory page is displayed
    And I select the enquiry about New Home Loans option and proceed
    Then the NAB call back form is loaded successfully in a new window
    And I enter the customer details in the call back form as
      | FirstName  | LastName   | State  |    PhoneNumber     |  Email                | ExistingCustomer  |NabID|
      |  Test      |   New      | NSW    |    0444444444      |  testnew@gmail.com    |      No           |     |
    Then I verify the call back form is submitted successfully

  Scenario: Verify the new home loan enquiry form is successfully submitted from the NAB website for an existing customer
    Given I launch the selected browser
    And I navigate to the NAB Home page
    Then the NAB Home page is loaded successfully
    When I click on the home loans link on the home page
    Then the home loans page is loaded successfully
    When I click on request a call back option on the home loan page
    Then the customer assistance directory page is displayed
    And I select the enquiry about New Home Loans option and proceed
    Then the NAB call back form is loaded successfully in a new window
    And I enter the customer details in the call back form as
      | FirstName  | LastName   | State  |    PhoneNumber     |  Email                | ExistingCustomer  |NabID        |
      |  Test      |   User     | QLD    |    0445544444      |  testuser@gmail.com   |      Yes          | 12457865    |
    Then I verify the call back form is submitted successfully
