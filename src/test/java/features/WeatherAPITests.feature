
Feature: Weather API Tests

  Background: User generates token for Authorisation
    Given I am an authorized user


  Scenario: Verify and validate the response of the GET request for the current weather data
     Given the latitude 40.730610 and longitude -73.935242 details for a region
     When the current weather endpoint is called
     And the API should return a valid response status
     Then the API response contains the state code as NY
