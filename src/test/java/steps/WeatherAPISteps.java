package steps;
import api.WeatherAPI;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;


public class WeatherAPISteps {

   public WeatherAPISteps(){

   }

    private String latitudeValue ="";
    private String longitudeValue ="";
    private Response response;

    @Given("I am an authorized user")
    public void iAmAnAuthorizedUser() {
        RestAssured.baseURI = System.getProperty("apiBaseUrl");
        System.out.println(System.getProperty("apiBaseUrl")+System.getProperty("apiKey"));
    }

    @Given("^the latitude (.*) and longitude (.*) details for a region$")
    public void latitudeAndLongitudeDetails(String latitude,String longitude) throws Throwable {
            latitudeValue = latitude;
            longitudeValue = longitude;
    }

    @When("^the current weather endpoint is called$")
    public void getCurrentWeatherEndpoint() throws Throwable {
        WeatherAPI wa=new WeatherAPI();
        response = wa.getCurrentWeather(latitudeValue,longitudeValue);
    }

    @Then("^the API should return a valid response status$")
    public void theAPIShouldReturnAResponse() throws Throwable {
        boolean statusCode = response.statusCode() == 200;
        Assert.assertTrue("API Response is not a success",statusCode);
    }
    @Then("^the API response contains the state code as (.*)$")
    public void theAPIResponseParser(String expectedStateCode) throws Throwable {
        String actualStateCode ="";
        JSONObject obj = new JSONObject(response.asString());
        JSONArray dataArray = obj.getJSONArray("data");
        for(int i = 0; i < dataArray.length() ; i++)
        {
            actualStateCode = dataArray.getJSONObject(i).getString("state_code");
        }
        Assert.assertTrue("Invalid state code from the weather api response",actualStateCode.equals(expectedStateCode));
    }

}
