package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import helper.XMLHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Hooks {

    public static WebDriver driver;
    public static int scenarioCounter = 1;

    @Before
    public static void setUp() throws Exception {
        XMLHelper.loadDefaultProperties();
        initializeDriver();
    }
    public static void initializeDriver() throws Exception {
        String browserName = System.getProperty("browser");
        if (scenarioCounter < 3)
        {
            scenarioCounter++;
            switch (browserName) {
                case "chrome":
                    System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chrome/chromedriver.exe");
                    driver = new ChromeDriver();
                    break;
                default:
                    throw new Exception("The browser: " + browserName + " is not supported yet.");
            }
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
    }

    @After
    public static void TearDown()
    {
        driver.quit();
    }

}
