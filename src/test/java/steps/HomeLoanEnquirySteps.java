package steps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.WindowHelper;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CustomerAssistancePage;
import pages.CustomerCallBackFormPage;
import pages.HomeLoanPage;
import pages.NabHomePage;

import java.util.*;
import java.util.logging.Logger;

public class HomeLoanEnquirySteps {
    WebDriver driver = Hooks.driver;
    private static Logger logger = Logger.getLogger(System.getProperty("browser"));
    private NabHomePage nabHomePageObject;
    private HomeLoanPage homeLoanPageObject;
    private CustomerAssistancePage customerAssistancePageObject;
    private CustomerCallBackFormPage customerCallBackFormPageObject;
    private WindowHelper windowHelperObject;
    private int EXPLICIT_TIMEOUT = 3;
    private String expectedSubmitMessage = "WE'VE RECEIVED YOUR REQUEST";

    public HomeLoanEnquirySteps() {
        nabHomePageObject = new NabHomePage(driver);
        homeLoanPageObject = new HomeLoanPage(driver);
        customerAssistancePageObject = new CustomerAssistancePage(driver);
        customerCallBackFormPageObject = new CustomerCallBackFormPage(driver);
        windowHelperObject = new WindowHelper(driver);
    }

    @Given("^I launch the selected browser$")
    public void iLaunchTheGivenBrowser() throws Throwable {
        logger.info("Test will be running on browser: " + System.getProperty("browser"));
    }

    @Given("^I navigate to the NAB Home page$")
    public void iNavigateToNabHomePage() throws Throwable {
        String nabUrl = System.getProperty("baseUrl");
        driver.get(nabUrl);
    }

    @Then("^the NAB Home page is loaded successfully$")
    public void isNabHomePageLoaded() throws Throwable {
        Assert.assertTrue("Failed to navigate to the Nab Home Page",
                driver.getTitle().contains("NAB Personal Banking"));
    }

    @When("^I click on the home loans link on the home page$")
    public void iClickOnHomeLoansLink() throws Throwable {
        WebElement personal = nabHomePageObject.getPersonalLink();
        if (new WebDriverWait(driver, EXPLICIT_TIMEOUT).until(condition -> personal.isDisplayed())) {
            personal.click();
        }
        List<WebElement> personalLinks = nabHomePageObject.getPersonalMenuLinks();
        for (int i = 0; i < personalLinks.size(); i++) {
            String linkName = personalLinks.get(i).getText();
            if (linkName.equalsIgnoreCase("Home loans")) {
                personalLinks.get(i).click();
            }
        }
        List<WebElement> homeLoanLinks = nabHomePageObject.getHomeLoanMenuLinks();
        for (int i = 0; i < homeLoanLinks.size(); i++) {
            String homeLoanLink = homeLoanLinks.get(i).getText();
            if (homeLoanLink.equalsIgnoreCase("Home loans")) {
                personalLinks.get(i).click();
            }
        }

    }


    @Then("^the home loans page is loaded successfully$")
    public void isHomeLoanPageLoaded() throws Throwable {
        Assert.assertTrue("Failed to navigate to the Home Loan Page",
                driver.getTitle().contains("Home loans"));
    }

    @When("^I click on request a call back option on the home loan page$")
    public void iClickOnRequestForCallBackOption() throws Throwable {
        WebElement callBack = homeLoanPageObject.getRequestCallBack();
        callBack.click();
    }

    @Then("^the customer assistance directory page is displayed$")
    public void isCustomerAssistanceDirectoryPageLoaded() throws Throwable {
        Assert.assertTrue("Failed to navigate to the Customer Assistance Directory Page",
                driver.getTitle().contains("Customer assistance directory"));
    }

    @Then("^I select the enquiry about (.*) option and proceed$")
    public void iSelectNewHomeLoanRadioButton(String option) throws Throwable {
        String expectedOption = option;
        List<WebElement> shadowRoot = customerAssistancePageObject.getListOfRadioButtons();
        for (int i = 0; i < shadowRoot.size(); i++) {
            String actualOption = shadowRoot.get(i).getText();
            if (expectedOption.equalsIgnoreCase(actualOption)) {
                shadowRoot.get(i).click();
                if (shadowRoot.get(i).isEnabled()) {
                    customerAssistancePageObject.getPagElement().sendKeys(Keys.CONTROL, Keys.END);
                    WebElement nextButton = customerAssistancePageObject.getNextButton();
                    Actions actions = new Actions(driver);
                    actions.moveToElement(nextButton).click().perform();
                    break;
                }
            }
        }
    }


    @Then("^the NAB call back form is loaded successfully in a new window$")
    public void isNabCallBackFormPageLoaded() throws Throwable {
        windowHelperObject.switchToChildWindow();
        Assert.assertTrue("Failed to navigate to the Nab Call back form page",
                driver.getTitle().contains("Callback Form"));
    }

    @Then("^I enter the customer details in the call back form as$")
    public void iEnterTheCustomerDetailsOnCallBackForm(DataTable customerDetails) throws Throwable {
        List<Map<String, String>> customerFormDetails = customerDetails.asMaps(String.class, String.class);
        for (Map<String, String> form : customerFormDetails) {

            String firstName = form.get("FirstName");
            String lastName = form.get("LastName");
            String state = form.get("State");
            String phoneNumber = form.get("PhoneNumber");
            String email = form.get("Email");
            String isNewCustomer = form.get("ExistingCustomer");
            String nabId = form.get("NabID");
            iFillTheCallBackForm(firstName, lastName, state, phoneNumber, email, isNewCustomer, nabId);
        }

    }

    public void iFillTheCallBackForm(String firstName, String lastName, String state, String phoneNumber,
                                     String email, String isNewCustomer, String nabId) throws InterruptedException {
        if (isNewCustomer.equalsIgnoreCase("No")) {
            WebElement nonExistingCustomer = customerCallBackFormPageObject.getCustomerToggleOptions(isNewCustomer);
            nonExistingCustomer.click();
            customerCallBackFormPageObject.getCustomerFirstName().sendKeys(firstName);
            customerCallBackFormPageObject.getCustomerLastName().sendKeys(lastName);
            customerCallBackFormPageObject.selectStateFromDropDown(state);
            customerCallBackFormPageObject.getCustomerPhoneNumber().sendKeys(phoneNumber);
            customerCallBackFormPageObject.getCustomerPhoneNumber().sendKeys(Keys.TAB);
            customerCallBackFormPageObject.getCustomerEmail().sendKeys(email);
            customerCallBackFormPageObject.getCustomerEmail().sendKeys(Keys.TAB);
            WebElement page = customerCallBackFormPageObject.getPageElement();
            page.sendKeys(Keys.CONTROL, Keys.END);
            int retryCount = 1;
            List<WebElement> submitButton = null;
            do {
                submitButton = customerCallBackFormPageObject.getSubmitButton();
                if (submitButton.size() > 0) {
                    submitButton.get(0).click();
                    break;
                }
                retryCount++;
            } while (retryCount < 10);
        } else if (isNewCustomer.equalsIgnoreCase("Yes")) {
            WebElement existingCustomer = customerCallBackFormPageObject.getCustomerToggleOptions(isNewCustomer);
            existingCustomer.click();
            customerCallBackFormPageObject.getNabId().sendKeys(nabId);
            customerCallBackFormPageObject.getCustomerFirstName().sendKeys(firstName);
            customerCallBackFormPageObject.getCustomerLastName().sendKeys(lastName);
            customerCallBackFormPageObject.selectStateFromDropDown(state);
            customerCallBackFormPageObject.getCustomerPhoneNumber().sendKeys(phoneNumber);
            customerCallBackFormPageObject.getCustomerPhoneNumber().sendKeys(Keys.TAB);
            customerCallBackFormPageObject.getCustomerEmail().sendKeys(email);
            customerCallBackFormPageObject.getCustomerEmail().sendKeys(Keys.TAB);
            WebElement page = customerCallBackFormPageObject.getPageElement();
            page.sendKeys(Keys.CONTROL, Keys.END);
            int retryCount = 1;
            List<WebElement> submitButton = null;
            do {
                submitButton = customerCallBackFormPageObject.getSubmitButton();
                if (submitButton.size() > 0) {
                    if (submitButton.get(0).isEnabled()) {
                        JavascriptExecutor jse = (JavascriptExecutor) driver;
                        jse.executeScript("arguments[0].click()", submitButton.get(0));
                        break;
                    }
                }
                retryCount++;
            }
            while (retryCount < 10);
        }
    }


    @Then("^I verify the call back form is submitted successfully$")
    public void iVerifyCallBackSubmitSuccessMessage() throws Throwable {
        int retryCount = 1;
        String actualMessage ="";
        List<WebElement> successMessage = null;
        do {
            successMessage = customerCallBackFormPageObject.getSuccessMessage();
            if(successMessage.size()>0){
                if(successMessage.get(0).isEnabled())
                    actualMessage= successMessage.get(0).getText();
                break;
            }
            retryCount++;
        } while (retryCount < 10);
            Assert.assertEquals("Failed to validate the call back form success message",
                    expectedSubmitMessage, actualMessage);
        }
    }


