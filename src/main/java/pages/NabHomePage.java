package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class NabHomePage extends BasePage {

    public NabHomePage(WebDriver driver)
    {
        super(driver);
    }


    @FindBy(linkText = "Personal")
    private WebElement personal;

    @FindBy(xpath = "//ul[@data-type='mega-menu']//span")
    private List<WebElement> personalMenu;

    @FindBy(css = ".nav-container span")
    private List<WebElement> homeLoanMenu;



    public WebElement getPersonalLink() {
        return personal;
    }

    public List<WebElement> getPersonalMenuLinks() {
        return personalMenu;
    }

    public List<WebElement> getHomeLoanMenuLinks() {
        return homeLoanMenu;
    }

}
