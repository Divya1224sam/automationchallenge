package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class CustomerAssistancePage extends BasePage{

    public CustomerAssistancePage(WebDriver driver)
    {
        super(driver);
    }


    @FindBy(css = "Buttonstyle__StyledButton-sc-1vu4swu-3.cchfek")
    private WebElement nextButton;

    @FindBy(css = "body")
    private WebElement pageElement;

    @FindBy(id = "contact-form-shadow-root")
    private WebElement shadowRoot;



    public List<WebElement> getListOfRadioButtons() {
        List<WebElement> radioButtons=getExpandShadowRootElement().
                findElements(By.cssSelector("[data-component-id=ControlGroup] [name=myRadioButton]"));
        return radioButtons;
    }

    public WebElement getNextButton() {
        WebElement nextButton = getExpandShadowRootElement().findElement(By.cssSelector("[type='button']"));
        return nextButton;
    }

    public WebElement clickNextButton() {
        WebElement nextButton = getExpandShadowRootElement().findElement(By.cssSelector("[type='button']"));
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(nextButton));
        return nextButton;
    }

    public WebElement getPagElement() {
        return pageElement;
    }

    public WebElement getExpandShadowRootElement() {
        WebElement ele = (WebElement) ((JavascriptExecutor)driver)
                .executeScript("return arguments[0].shadowRoot", shadowRoot);
        return ele;
    }
}
