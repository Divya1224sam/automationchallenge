package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomeLoanPage extends BasePage {

    public HomeLoanPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(linkText = "Request a call back")
    private WebElement RequestCallBack;

    public WebElement getRequestCallBack() {
        return RequestCallBack;
    }
}