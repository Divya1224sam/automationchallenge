package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CustomerCallBackFormPage extends BasePage {

    public CustomerCallBackFormPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "[type=radio] span")
    private List<WebElement> customer;

    @FindBy(id = "field-page-Page1-aboutYou-firstName")
    private WebElement firstName;

    @FindBy(id = "field-page-Page1-aboutYou-lastName")
    private WebElement lastName;

    @FindBy(xpath = "//*[text()='Select state']")
    private WebElement selectDropdown;

    @FindBy(id = "field-page-Page1-aboutYou-phoneNumber")
    private WebElement phoneNumber;

    @FindBy(id = "field-page-Page1-aboutYou-email")
    private WebElement email;

    @FindBy(id = "page-Page1-btnGroup-submitBtn")
    private List<WebElement> submitButton;

    @FindBy(id = "field-page-Page1-nabID")
    private WebElement nabId;

    @FindBy(css = "#page-outcome-rowthankyoupageheader h1")
    private List<WebElement> successMessage;

    @FindBy(css = "body")
    private WebElement pageElement;

    public WebElement getCustomerToggleOptions(String customerType) {
        WebElement customerToggleOption = null;
        if (customerType.equalsIgnoreCase("Yes")) {
            customerToggleOption = customer.get(0);
        } else if (customerType.equalsIgnoreCase("No")) {
            customerToggleOption = customer.get(1);
        }
        return customerToggleOption;

    }

    public WebElement getCustomerFirstName()
    {
        return firstName;
    }

    public WebElement getCustomerLastName()
    {
        return lastName;
    }

    public WebElement getPageElement()
    {
        return pageElement;
    }

    public void selectStateFromDropDown(String state)
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,350)", "");
        selectDropdown.click();
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable
                ((By.xpath("//div[contains(@class, 'select__menu')]/div[contains(@class, 'select__menu-list')]//div[contains(@class, 'select__option') and text()='"+state+"']")))).click();

    }

    public WebElement getCustomerPhoneNumber()
    {
        return phoneNumber;
    }

    public WebElement getCustomerEmail()
    {
        return email;
    }

    public List<WebElement> getSubmitButton()
    {
        return submitButton;
    }
    public WebElement getNabId()
    {
        return nabId;
    }

    public List<WebElement> getSuccessMessage()
    {
        return successMessage;
    }
}
