#### Introduction
The Automation challenge project automation framework has been developed using **Java**, **Selenium** , 
**Page Object Model** and **Page Factory** by following BDD approach using **Cucumber** as a **MavenProject**
and the API tests using  **RestAssured** 

#### Automation workflow
1.Feature File which contains the BDD implementation of the given scenario is under the  src/test/features folder
2.Step Definition file which implements the lines of the feature file is under the src/test/steps folder
3.Page Object file which contains the page objects used for the scenario execution is under the src/main/pages folder
4.Test runner file which is used to execute the scenario in the feature file is under the src/test/runners folder
5.The base URLs for both UI and API tests along the browser details are in the config file under the src/test/properties folder
6.Helpers file contain methods that aid with the execution for reading the values from config file and handling multiple windows
under the src/test/helper folder.
7.Chrome driver is under the drivers folder
8.pom.xml file has got all the dependencies and plugins required to set up and execute the scenario

#### Steps to set up and run the test scenario
1.Install JDK and  set up the path of JDK in the system variables under JAVA_HOME and path variables
2.Install MAVEN and set up the path for maven in the system variables under path variables
3.Download and install any IDE (Eclipse / IntelliJ) depending on the operating system.
4.Set up the Java SDK path to the IDE
5.Clone the project from BitBucket repository 
6.Open the project as a maven project in the IDE
7.Dependencies and plugins required for the test execution in the **pom.xml** file 
 should automatically be installed. 
8.The base URLs and the browser to be used can be specified in the **default_config.xml** file
9.Test run  starts by running the **"RunnerTest"** file.
